Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

M. Peter
Implementierung eines numerischen Modells für den Transport von Tracerkomponenten in Zweiphasenströmungen
Projektarbeit, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 01/2019.


Installation
============
The content of this DUNE module was extracted from the module dumux.
In particular, the following subfolders of dumux have been extracted:

  test/porousmediumflow/tracer/2ptracer/,

Additionally, all headers in dumux that are required to build the
executables from the sources

  test/porousmediumflow/tracer/2ptracer/test_2pnc.cc,
  test/porousmediumflow/tracer/2ptracer/test_2p_cctracer.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

dune-common               releases/2.6    f3cb9408388f3424b4f0a7a55cad84057b6b444d
dune-istl                 releases/2.6    9698e497743654b6a03c219b2bdfc27b62a7e0b3
dune-geometry             releases/2.6    1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
dune-grid                 releases/2.6    76f18471498824d49a6cecbfba520b221d9f79ca
dune-localfunctions       releases/2.6    ee794bfdfa3d4f674664b4155b6b2df36649435f
dumux                     master          3a232ad4e79ec55960c35f2dcfcfccaa76094e24


Applications
============

The results in the master's thesis are obtained by compiling and running the programs from the sources listed below, which can be found in

.../test/porousmediumflow/tracer/2ptracer :

test_2pnc.cc		-> make test_2pnc
			-> ./test_2pnc
test_2p_cctracer.cc	-> make test_2p_tracer_sequential
			-> ./test_2p_tracer_sequential tracer_2p.input (the input file might need to be adapted)


Output
======

Run the program with the respective source file modifications and input-files in the same folder:


* __test_2pnc__:

    __Figure 3.2_2pnc(left) - distribution of molefraction x_NaCl(t_end)__:
        - comment in #include "2pnctestproblem_3stripes.hh" (line 36, test_2pnc.cc)
	- comment out #include "2pnctestproblem.hh" (line 35, test_2pnc.cc)
	- comment in #include "2ptestspatialparams.hh" (line 40, 2pnctestproblem_3stripes.hh)
	- comment out #include "2ptestspatialparams_randomfield.hh" (line 41, 2pnctestproblem_3stripes.hh)
	(- add tracercomponent indices only for tracer1Idx (line 122)
	   and PrimaryVariables dirichletAtPos only the values for tracer1Idx (line 197, 209 same as 286, 298)
	   (2pnctestproblem_3stripes.hh) )
        - set TEnd = 5000 # [s] and MaxTimeStepSize = 100 in input-file (test_2pnc.input)

    __Figure 3.6_2pncPlot - "plot over line"(x_NaCl)__:  
	- run the program as for Figure 3.2_2pnc
	- plot x^NaCl1_aq over line to according position (and t=0,1000,5000)

	(- paraview -> Filters -> Data Analysis -> Plot Over Line
	- set Line Parameters to y-Axis and Point1_x = Point_x = 4.05
	- seperate only x^NaCl1_aq from the Series Parameters and the graphs for t=0,1000,5000)

    __Figure 3.7_2pncPlot(top) - cellplot over time for t_max=100__:
	- run the program as for Figure 3.2_2pnc
	- plot x^NaCl1_aq over time for selected cell

	(- paraview -> select cell XX -> Filter -> Data Analysis -> Plot Selection over time
	- seperate only x^NaCl1_aq from the Series Parameters)

    __Figure 3.7_2pncPlot(bottom) - cellplot over time for t_max=10__:
	- see Figure 3.7_2pncPlot(top) except MaxTimeStepSize = 10 in input-file (test_2pnc.input)

    __Figure 3.9_2pncPlot - total runtime(number of tracercomponents)__:
	- comment in #include "2pnctestproblem.hh" (line 35, test_2pnc.cc)
        - comment out #include "2pnctestproblem_3stripes.hh" (line 36, test_2pnc.cc)
	- comment in #include "2ptestspatialparams_randomfield.hh" (line 41, 2pnctestproblem.hh)
	- comment out #include "2ptestspatialparams.hh" (line 40, 2pnctestproblem.hh)
	- add tracercomponent indices (line 122) and PrimaryVariables dirichletAtPos values (line 195, 207 same as 282, 294) for tracer2Idx,... depending the number of tracercomponents (2pnctestproblem.hh)
	- adapt numTracers (line 74) and the component index (line 82), componentName (line 244)(2pncimmiscible.hh)
	- set TEnd = 500 # [s] and MaxTimeStepSize = 10 in input-file (test_2pnc.input)


* __test_2pcctracer__:

    __Figure 3.2_2ptracer(right) - distribution of molefraction x_NaCl(t_end)__:  
        - comment in #include "2ptracertestproblem_3stripes.hh" (line 28, test_2p_cctracer.cc)
	- comment out #include "2ptracertestproblem.hh" (line 27, test_2p_cctracer.cc)
	- comment in #include "2ptestspatialparams.hh" (line 41, 2ptestproblem.hh)
	- comment out #include "2ptestspatialparams_randomfield.hh" (line 42, 2ptestproblem.hh)
	(- set numComponents = 1 (line 94, 2ptracertestproblem_3stripes.hh) )
        - set TEnd = 5000 # [s] and MaxTimeStepSize = 100 in input-file (tracer_2p.input)


    __Figure 3.6_2ptracerPlot - "plot over line"(x_NaCl)__:
	- run the program as for Figure 3.2_2ptracer  
        - paraview -> Filters -> Data Analysis -> Plot Over Line
	- set Line Parameters to y-Axis and Point1_x = Point_x = 4.05
	- seperate only x_Water^tracer_0 from the Series Parameters and the graphs for t=0,1000,5000

    __Figure 3.7_2ptracerPlot(oben) - cellplot over time for t_max=100__:
	- run the program as for Figure 3.2_2pnc  
	- paraview -> select cell XX -> Filter -> Data Analysis -> Plot Selection over time
	- seperate only x^NaCl1_aq from the Series Parameters

    __Figure 3.7_2ptracerPlot(unten) - cellplot over time for t_max=10__:
	- see Figure 3.7_2ptracerPlot(oben) except MaxTimeStepSize = 10 in input-file (test_2pnc.input)

    __Figure 3.9_2ptracer - total runtime(number of tracercomponents)__:
        - comment in #include "2ptracertestproblem_3stripes.hh" (line 28, test_2p_cctracer.cc)
	- comment out #include "2ptracertestproblem.hh" (line 27, test_2p_cctracer.cc)
	- comment in #include "2ptestspatialparams.hh" (line 41, 2ptestproblem.hh)
	- comment out #include "2ptestspatialparams_randomfield.hh" (line 42, 2ptestproblem.hh)
	- set numComponents = 1,...,8 (line 94, 2ptracertestproblem.hh)
        - set TEnd = 500 # [s] and MaxTimeStepSize = 10 in input-file (tracer_2p.input)
	-> The cumulative CPU time was [--] seconds. (output in the terminal)



__Figure 3.3 - difference(x_NaCl) between Figure 3.2_2pnc(left) and Figure 3.2_2ptracer(right)  (3.2)__:
	- use the data from Figure 3.2_2pnc(left) and Figure 3.2_2ptracer(right) and compute the err_x^NaCl = |x_nc − x_tracer|

	(- paraview -> Filter -> Data Analysis -> Programmable Filter -> err_x^NaCl = |x_nc − x_tracer|)


__Figure 3.4 - difference(velocity(magnitude))between Figure 3.2_2pnc(left) and Figure 3.2_2ptracer(right)  (3.2)__:
	- use the data from Figure 3.2_2pnc(left) and Figure 3.2_2ptracer(right) and compute the err_vel(magnitude) = |vel_nc − vel_tracer|


	(- paraview -> Filter -> Data Analysis -> Programmable Filter -> err_vel(magnitude) = |vel_nc − vel_tracer|)



Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Peter2019a
cd Peter2019a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/peter2019a/-/raw/master/docker_peter2019a.sh
```

Open the Docker Container
```bash
bash docker_peter2019a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd peter2019a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake/test/porousmediumflow/tracer/2ptracer folder. They can be executed with an input file e.g., by running

```
cd test/porousmediumflow/tracer/2ptracer
./test_2pnc test_2pnc.input
```
