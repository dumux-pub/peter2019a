add_subdirectory("1p")
add_subdirectory("2p")

install(FILES
        diffusivityconstanttortuosity.hh
        diffusivitymillingtonquirk.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/fluidmatrixinteractions)
