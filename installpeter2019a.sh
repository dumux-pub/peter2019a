# dune-common
# releases/2.6 # f3cb9408388f3424b4f0a7a55cad84057b6b444d # 2018-12-13 17:16:34 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.6
git reset --hard f3cb9408388f3424b4f0a7a55cad84057b6b444d
cd ..

# dune-istl
# releases/2.6 # 9698e497743654b6a03c219b2bdfc27b62a7e0b3 # 2018-12-14 10:00:30 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.6
git reset --hard 9698e497743654b6a03c219b2bdfc27b62a7e0b3
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.6
git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
cd ..

# dune-uggrid
# releases/2.6 # 07f9700459c616186737a9a34277f2edee76f475 # 2018-04-04 16:53:52 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
cd dune-uggrid
git checkout releases/2.6
git reset --hard 07f9700459c616186737a9a34277f2edee76f475
cd ..

# dune-grid
# releases/2.6 # 76f18471498824d49a6cecbfba520b221d9f79ca # 2018-12-10 14:35:11 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.6
git reset --hard 76f18471498824d49a6cecbfba520b221d9f79ca
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.6
git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f
cd ..

# dune-python
# releases/2.6 # 98bcccf5e841b5eb2a8ab58ca06915bd7930d2c8 # 2018-11-21 08:12:02 +0000 # dedner
git clone https://gitlab.dune-project.org/staging/dune-python.git
cd dune-python
git checkout releases/2.6
git reset --hard 98bcccf5e841b5eb2a8ab58ca06915bd7930d2c8
cd ..

# dune-alugrid
# releases/2.6 # c0851a92b3af8d93a75f798de3d34f65cc895341 # 2018-07-25 11:01:08 +0000 # Martin Alkämper
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.6
git reset --hard c0851a92b3af8d93a75f798de3d34f65cc895341
cd ..

# dune-foamgrid
# releases/2.6 # 975458f8dbcae07a682c03961f5dcc6cf54ebd67 # 2018-07-23 15:25:01 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.6
git reset --hard 975458f8dbcae07a682c03961f5dcc6cf54ebd67
cd ..

# dune-subgrid
# releases/2.6-1 # 6ef4ab8198ad707af9fc7bacec279d3c7c53d4c5 # 2018-08-02 10:00:58 +0200 # Jonathan Youett
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git checkout releases/2.6-1
git reset --hard 6ef4ab8198ad707af9fc7bacec279d3c7c53d4c5
cd ..

# dune-spgrid
# releases/2.6 # dbfa7f2a34a0a6e440d602ce0651c28dc3cdf07e # 2018-07-05 19:02:55 +0000 # Martin Nolte
git clone https://gitlab.dune-project.org/extensions/dune-spgrid
cd dune-spgrid
git checkout releases/2.6
git reset --hard dbfa7f2a34a0a6e440d602ce0651c28dc3cdf07e
cd ..

# dumux
# master # 3a232ad4e79ec55960c35f2dcfcfccaa76094e24
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard 3a232ad4e79ec55960c35f2dcfcfccaa76094e24
cd ..

# peter2019a
# master # 
git clone https://git.iws.uni-stuttgart.de/dumux-pub/peter2019a.git
cd peter2019a
git checkout master
cd ..

